import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { NowPlayingListComponent } from './components/now-playing-list/now-playing-list.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TrucComponent } from './components/truc/truc.component';
import { SimilarComponentComponent } from './components/similar-component/similar-component.component';



const routes: Routes = [
  {path:"", pathMatch:"full", component: HelloWorldComponent},
  {path:"now-playing", component: NowPlayingListComponent},
  {path:"not-found", component : NotFoundComponent},
  {path:"movie-detail/:id", component : TrucComponent},
  {path:"similar-movie/:id", component : SimilarComponentComponent},
  {path:"**", redirectTo:"/not-found"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
