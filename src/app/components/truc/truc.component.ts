import { Component, OnInit } from '@angular/core';
import { MovieDBService } from 'src/app/services/movie-db.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-truc',
  templateUrl: './truc.component.html',
  styleUrls: ['./truc.component.css']
})
export class TrucComponent implements OnInit {

  player : YT.Player;
  movieTrailer : any;

  constructor(public movieDbService : MovieDBService, private route: ActivatedRoute) { }

  ngOnInit() {
    const movieId = this.route.snapshot.paramMap.get("id")

    console.log(movieId)
    
    this.movieDbService.getMovieVideo(+movieId).subscribe((res:any)=>{
      console.log(res);
      this.movieTrailer = res.results;
    }, (err)=>{
      console.log(err);
    })
  }

  savePlayer(player) {
    this.player = player;
    console.log('player instance', player);
  }
  onStateChange(event) {
    console.log('player state', event.data);
  }

}
