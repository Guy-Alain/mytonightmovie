import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieDBService {

  API_KEY = 'api_key=2554efe946e607bc226f6bd9800cbeac';
  BASE_URL = 'https://api.themoviedb.org/3/movie';
  BASE_URL_IMG = 'https://image.tmdb.org/t/p/w500';

  constructor(private http:HttpClient) { }

  public getNowPlayingMovies() {
    return this.http.get(`${this.BASE_URL}/now_playing?${this.API_KEY}`);
  }

  public getMovieDbImg(imgName:String) {
    return `${this.BASE_URL_IMG}${imgName}`
  }

  public getMovieVideo(movieID:number) {
    return this.http.get(`${this.BASE_URL}/${movieID}/videos?${this.API_KEY}`);
  }

  public getSimilarMovie(movieID:number){
    return this.http.get(`${this.BASE_URL}/${movieID}/similar?${this.API_KEY}`)
  }






}
