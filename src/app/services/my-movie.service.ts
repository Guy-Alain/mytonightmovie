import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyMovieService {

  SPRING_URL = "http://localhost:8090/api/";

  constructor(private http: HttpClient) { }

  getAllFavoriteMovie() {
    return this.http.get(`${this.SPRING_URL}/films`);
  }

  addToFavorite(film : any) {
    return this.http.post(`${this.SPRING_URL}/films`, film);
  }
}
